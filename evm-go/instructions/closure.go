/*
 * Copyright 2020 The SealEVM Authors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package instructions

import (
	"encoding/hex"
	"errors"

	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/params"

	"chainmaker.org/chainmaker/common/v2/evmutils"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/opcodes"
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/precompiledContracts"
	"chainmaker.org/chainmaker/vm-evm/v2/evm-go/storage"
)

type ClosureExecute func(ClosureParam) ([]byte, error)

type ClosureParam struct {
	VM           interface{}
	OpCode       opcodes.OpCode
	GasRemaining *evmutils.Int

	ContractAddress *evmutils.Int
	ContractHash    *evmutils.Int
	ContractCode    []byte

	CallData   []byte
	CallValue  *evmutils.Int
	CreateSalt *evmutils.Int
}

func loadClosure() {
	//Normal call
	instructionTable[opcodes.CALL] = opCodeInstruction{
		action:            callAction,
		requireStackDepth: 7,
		enabled:           true,
		returns:           true,
	}

	//Use only the caller's code and keep the execution context of the current contract
	instructionTable[opcodes.CALLCODE] = opCodeInstruction{
		action:            callCodeAction,
		requireStackDepth: 7,
		enabled:           true,
		returns:           true,
	}

	//Equivalent to callcode but reserving the caller and callValue
	instructionTable[opcodes.DELEGATECALL] = opCodeInstruction{
		action:            delegateCallAction,
		requireStackDepth: 6,
		enabled:           true,
		returns:           true,
	}

	//Static call, the caller does not modify the caller's own state after execution
	instructionTable[opcodes.STATICCALL] = opCodeInstruction{
		action:            staticCallAction,
		requireStackDepth: 6,
		enabled:           true,
		returns:           true,
	}

	//Addresses are automatically created before version 2300,
	//and contract names in address format are automatically created after version 2300
	instructionTable[opcodes.CREATE] = opCodeInstruction{
		action:            createAction,
		requireStackDepth: 3,
		enabled:           true,
		returns:           true,
		isWriter:          true,
	}

	//Before version 2300, you can add salt value in the parameter of generating address. After version 2300,
	//the parameter of salt value is changed to allow users to pass contract name
	instructionTable[opcodes.CREATE2] = opCodeInstruction{
		action:            create2Action,
		requireStackDepth: 2,
		enabled:           true,
		returns:           true,
		isWriter:          true,
	}
}

func commonCall(ctx *instructionsContext, opCode opcodes.OpCode) ([]byte, error) {
	_ = ctx.stack.Pop()
	addr := ctx.stack.Pop()
	var v *evmutils.Int = nil

	if opCode != opcodes.DELEGATECALL && opCode != opcodes.STATICCALL {
		//Neither delegate invocation nor static invocation modifies the state of the invoked contract,
		//so value is not required
		v = ctx.stack.Pop()
	}

	//DOffset and dLen are used to read callData
	dOffset := ctx.stack.Pop()
	dLen := ctx.stack.Pop()
	//ROffset and rLen are used to read the return value
	rOffset := ctx.stack.Pop()
	rLen := ctx.stack.Pop()

	//gas check
	offset, size, gasLeft, err := ctx.memoryGasCostAndMalloc(dOffset, dLen)
	if err != nil {
		return nil, err
	}

	//read calldata
	data, err := ctx.memory.Copy(offset, size)
	//view data
	if err != nil {
		return nil, err
	}

	//read code
	contractCode, err := ctx.storage.GetCode(addr)
	if err != nil {
		return nil, err
	}

	var callRet []byte
	version := ctx.storage.GetCurrentBlockVersion()
	if version < params.V2300 || opCode == opcodes.DELEGATECALL || opCode == opcodes.CALLCODE ||
		(version >= params.V2030100 && precompiledContracts.IsPrecompiledContract(addr)) {
		//If the version is smaller than 2300, a separate VM instance is created in the VM to execute i
		//the invoked contract
		contractCodeHash, err := ctx.storage.GetCodeHash(addr)
		if err != nil {
			return nil, err
		}

		cParam := ClosureParam{
			VM:              ctx.vm,
			OpCode:          opCode, //Tell the new virtual machine instance the call type
			GasRemaining:    ctx.gasRemaining,
			ContractAddress: addr,
			ContractCode:    contractCode,
			ContractHash:    contractCodeHash,
			CallData:        data,
			CallValue:       v,
		}

		callRet, err = ctx.closureExec(cParam)
		if err != nil {
			return nil, err
		}
	} else {
		//Cross-contract calls greater than 2300 board will go Chainmaker-go
		var method string
		if len(data) >= 4 {
			method = hex.EncodeToString(data)[0:8]
		}

		name := hex.EncodeToString(addr.Bytes())
		sender := hex.EncodeToString(ctx.environment.Contract.Address.Bytes())
		gasUsed := ctx.environment.Block.GasLimit.Uint64() - gasLeft

		parameter := make(map[string][]byte)
		//Add cross-contract invocation parameters
		//calldata
		parameter[protocol.ContractEvmParamKey] = []byte(hex.EncodeToString(data))
		//The current contract is called the caller of the called contract
		parameter[syscontract.CrossParams_SENDER.String()] = []byte(sender)
		//Indicates whether the current invocation is a user or cross-contract invocation
		parameter[syscontract.CrossParams_CALL_TYPE.String()] = []byte(syscontract.CallType_CROSS.String())
		//The invocation contract does not require the vm type, only for deployment
		result, status := ctx.storage.ExternalStorage.CallContract(name, 0, method, contractCode, parameter,
			gasUsed, false)
		if status != common.TxStatusCode_SUCCESS || result.Code != 0 {
			if ctx.storage.GetCurrentBlockVersion() < params.V2030100 {
				return nil, err
			} else {
				return nil, errors.New(result.Message)
			}
		}

		ctx.storage.ResultCache.ContractEvent = append(ctx.storage.ResultCache.ContractEvent, result.ContractEvent...)
		gasLeft -= result.GasUsed
		ctx.gasRemaining.SetUint64(gasLeft)
		callRet = result.Result
	}

	//gas check
	offset, size, _, err = ctx.memoryGasCostAndMalloc(rOffset, rLen)
	if err != nil {
		return nil, err
	}

	//Write the result to memory
	err = ctx.memory.StoreNBytes(offset, size, callRet)
	if err != nil {
		//Presses the execution successfully or not
		ctx.stack.Push(evmutils.New(0))
	} else {
		ctx.stack.Push(evmutils.New(1))
	}
	return callRet, err
}

func callAction(ctx *instructionsContext) ([]byte, error) {
	return commonCall(ctx, opcodes.CALL)
}

func callCodeAction(ctx *instructionsContext) ([]byte, error) {
	return commonCall(ctx, opcodes.CALLCODE)
}

func delegateCallAction(ctx *instructionsContext) ([]byte, error) {
	return commonCall(ctx, opcodes.DELEGATECALL)
}

func staticCallAction(ctx *instructionsContext) ([]byte, error) {
	return commonCall(ctx, opcodes.STATICCALL)
}

func commonCreate(ctx *instructionsContext, opCode opcodes.OpCode) ([]byte, error) {
	value := ctx.stack.Pop()
	mOffset := ctx.stack.Pop()
	mSize := ctx.stack.Pop()
	//rand.Seed(time.Now().UnixNano())

	var name string
	var addr *evmutils.Int
	var rtType int32 = int32(common.RuntimeType_EVM)
	var salt *evmutils.Int = evmutils.New(0)
	if ctx.storage.GetCurrentBlockVersion() < 2300 {
		if opCode == opcodes.CREATE2 {
			//The create2 directive has an extra user-assigned salt value
			salt = salt.Add(ctx.stack.Pop())
		}

		//The bytecode that the contract executes to this point is used as one of the parameters to calculate
		//the address
		solt0 := evmutils.New(0).SetBytes(ctx.environment.Contract.Code[0:ctx.pc])
		salt.Add(evmutils.FromBigInt(solt0))
		//The address is obtained by hashing the caller, salt, and transaction
		addr = ctx.storage.CreateFixedAddress(ctx.environment.Message.Caller, salt, ctx.environment.Transaction,
			ctx.environment.Cfg.AddrType)
	} else {
		if opCode == opcodes.CREATE2 {
			//In versions greater than 2300, salt is modified to the name of the created contract that can be
			//assigned by the user
			customize := ctx.stack.Pop().Bytes()
			rtType = int32(customize[0])
			customize = customize[1:]
			salt.SetBytes(customize)
			//addr = ctx.storage.CreateAddress(salt, ctx.environment.Cfg.AddrType)
			bName := storage.TruncateNullTail(customize)
			name = string(bName)
		} else {
			//The CREATE command has no salt parameter
			salt.SetBytes(ctx.environment.Contract.Code[0:ctx.pc])
			fixAddress := ctx.storage.CreateFixedAddress(ctx.environment.Message.Caller, salt, ctx.environment.Transaction,
				ctx.environment.Cfg.AddrType)
			//The CREATE command has no salt parameter, so only one address is automatically calculated as the name,
			//and subsequent operations will use that address as the name to calculate the address
			name = hex.EncodeToString(fixAddress.Bytes())
		}
	}

	//gas check
	offset, size, gasLeft, err := ctx.memoryGasCostAndMalloc(mOffset, mSize)
	if err != nil {
		return nil, err
	}

	//read code
	code, err := ctx.memory.Copy(offset, size)
	if err != nil {
		return nil, err
	}

	var ret []byte
	if ctx.storage.GetCurrentBlockVersion() < 2300 {
		//For use prior to 2300, since cross-contract calls are done within VM-EVM
		ctx.storage.SetCode(addr, code)
		hash := evmutils.Keccak256(code)
		i := evmutils.New(0)
		i.SetBytes(hash)
		ctx.storage.SetCodeHash(addr, i)
		ctx.storage.SetCodeSize(addr, evmutils.New(int64(len(code))))

		//Versions less than 2300 are still created using EVM
		cParam := ClosureParam{
			VM:              ctx.vm,
			OpCode:          opCode,
			GasRemaining:    ctx.gasRemaining,
			ContractAddress: addr,
			ContractCode:    code,
			CallData:        []byte{},
			CallValue:       value,
			CreateSalt:      salt,
		}

		ret, err = ctx.closureExec(cParam)
	} else {
		//Version creation logic greater than 2300 goes through Chainmaker-go
		gasUsed := ctx.environment.Block.GasLimit.Uint64() - gasLeft

		parameter := make(map[string][]byte, 2)
		//Sets cross-contract invocation parameters
		sender := hex.EncodeToString(ctx.environment.Contract.Address.Bytes())
		//The sender has become the current contract
		parameter[syscontract.CrossParams_SENDER.String()] = []byte(sender)
		//Indicates that the call type is a cross-contract call
		parameter[syscontract.CrossParams_CALL_TYPE.String()] = []byte(syscontract.CallType_CROSS.String())
		result, status := ctx.storage.ExternalStorage.CallContract(name, rtType,
			protocol.ContractInitMethod, code, parameter, gasUsed, true)
		if status != common.TxStatusCode_SUCCESS || result.Code != 0 {
			if ctx.storage.GetCurrentBlockVersion() < params.V2030100 {
				err = errors.New(result.Message)
				//Ret other than the RETURN command is ignored
				ret = result.Result
			} else {
				return ret, errors.New(result.Message)
			}
		}

		ctx.storage.ResultCache.ContractEvent = append(ctx.storage.ResultCache.ContractEvent, result.ContractEvent...)
		var contract common.Contract
		if err := contract.Unmarshal(result.Result); err != nil {
			return nil, err
		}
		addr = evmutils.FromHexString(contract.Address)

		gasLeft -= result.GasUsed
		ctx.gasRemaining.SetUint64(gasLeft)
	}

	if err != nil {
		ctx.stack.Push(evmutils.New(0))
	} else {
		//addr:=ctx.storage.CreateFixedAddress(ctx.environment.Message.Caller,salt,ctx.environment.Transaction)
		ctx.stack.Push(addr)
	}

	//The RET of all instructions except RETURN is ignored
	return ret, err
}

func createAction(ctx *instructionsContext) ([]byte, error) {
	return commonCreate(ctx, opcodes.CREATE)
}

func create2Action(ctx *instructionsContext) ([]byte, error) {
	return commonCreate(ctx, opcodes.CREATE2)
}
