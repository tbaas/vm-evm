/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package evm

import (
	"encoding/hex"
	"io/ioutil"
	"strings"
	"testing"

	"chainmaker.org/chainmaker/common/v2/evmutils/abi"

	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/utils/v2"

	"chainmaker.org/chainmaker/logger/v2"
	pbac "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	configPb "chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/vm-evm/v2/test"
)

const (
	chainId      = "chain01"
	OrgId1       = "wx-org1.chainmaker.org"
	certFilePath = "./test/config/admin1.sing.crt"
	txId         = "TX_ID_XXX"
	//storeBinFile = "./test/contracts/contract02/Storage.bin"
	//storeAbiFile = "./test/contracts/contract02/Storage.abi"
	//storeName    = "storeContract"

	contractVersion = "v1.0.0"
	initMethod      = "init_contract"
	tokenName       = "contract_token"
	tokenBinPath    = "./test/contracts/contract01/token.bin"
	tokenBodyFile   = "./test/contracts/contract01/token_body.bin"
	//contractStoreName  = "contract_store"
	//storeBodyFile      = "./test/contracts/contract02/storage_body.bin"

	contractCName = "contract_C"
	contractCBin  = "./test/contracts/contract03/C.bin"
	contractCBody = "./test/contracts/contract03/C_body.bin"
	contractCAbi  = "./test/contracts/contract03/C.abi"
)

func TestInstallContractToken(t *testing.T) {
	//部署合约
	method := initMethod
	test.CertFilePath = certFilePath
	test.ContractName = tokenName
	test.ByteCodeFile = tokenBinPath
	parameters := make(map[string][]byte)
	contractId, txContext, byteCode := test.InitContextTest(common.RuntimeType_EVM, t)

	runtimeInstance := &RuntimeInstance{
		ChainId:      chainId,
		Log:          logger.GetLogger(logger.MODULE_VM),
		TxSimContext: txContext,
	}

	loggerByChain := logger.GetLoggerByChain(logger.MODULE_VM, chainId)

	byteCode, _ = hex.DecodeString(string(byteCode))
	test.BaseParam(parameters)
	parameters[protocol.ContractCreatorPkParam] = contractId.Creator.MemberInfo
	parameters[protocol.ContractSenderPkParam] = txContext.GetSender().MemberInfo
	parameters["data"] = []byte("00000000000000000000000013f0c1639a9931b0ce17e14c83f96d4732865b58")
	contractResult, _ := runtimeInstance.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	loggerByChain.Infof("ContractResult Code:%+v", contractResult.Code)
	loggerByChain.Infof("ContractResult ContractEvent:%+v", contractResult.ContractEvent)
	loggerByChain.Infof("ContractResult GasUsed:%+v", contractResult.GasUsed)
	loggerByChain.Infof("ContractResult Message:%+v", contractResult.Message)
	loggerByChain.Infof("ContractResult Result:%+X", contractResult.Result)
}

func TestInvokeToken(t *testing.T) {
	//调用合约
	method := "4f9d719e" //method testEvent
	test.ContractName = tokenName
	test.ByteCodeFile = tokenBodyFile
	test.CertFilePath = certFilePath
	parameters := make(map[string][]byte)
	contractId, txContext, byteCode := test.InitContextTest(common.RuntimeType_EVM, t)

	runtimeInstance := &RuntimeInstance{
		ChainId:      chainId,
		Log:          logger.GetLogger(logger.MODULE_VM),
		TxSimContext: txContext,
	}

	loggerByChain := logger.GetLoggerByChain(logger.MODULE_VM, chainId)

	byteCode, _ = hex.DecodeString(string(byteCode))
	test.BaseParam(parameters)
	parameters[protocol.ContractCreatorPkParam] = contractId.Creator.MemberInfo
	parameters[protocol.ContractSenderPkParam] = txContext.GetSender().MemberInfo
	parameters["data"] = []byte("4f9d719e")

	contractResult, _ := runtimeInstance.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	loggerByChain.Infof("method testEvent-- ContractResult Code:%+v", contractResult.Code)
	loggerByChain.Infof("method testEvent-- ContractResult ContractEvent:%+v", contractResult.ContractEvent)
	loggerByChain.Infof("method testEvent-- ContractResult GasUsed:%+v", contractResult.GasUsed)
	loggerByChain.Infof("method testEvent-- ContractResult Message:%+v", contractResult.Message)
	loggerByChain.Infof("method testEvent-- ContractResult Result:%+X", contractResult.Result)
}

func TestInstallC(t *testing.T) {
	//部署合约
	method := initMethod
	test.ContractName = contractCName
	test.CertFilePath = certFilePath
	test.ByteCodeFile = contractCBin
	parameters := make(map[string][]byte)
	contractId, txContext, byteCode := test.InitContextTest(common.RuntimeType_EVM, t)

	runtimeInstance := &RuntimeInstance{
		ChainId:      chainId,
		Log:          logger.GetLogger(logger.MODULE_VM),
		TxSimContext: txContext,
	}

	loggerByChain := logger.GetLoggerByChain(logger.MODULE_VM, chainId)

	byteCode, _ = hex.DecodeString(string(byteCode))
	test.BaseParam(parameters)
	parameters[protocol.ContractCreatorPkParam] = contractId.Creator.MemberInfo
	parameters[protocol.ContractSenderPkParam] = txContext.GetSender().MemberInfo
	//parameters["data"] = []byte("00000000000000000000000013f0c1639a9931b0ce17e14c83f96d4732865b58")
	contractResult, _ := runtimeInstance.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	loggerByChain.Infof("ContractResult Code:%+v", contractResult.Code)
	loggerByChain.Infof("ContractResult ContractEvent:%+v", contractResult.ContractEvent)
	loggerByChain.Infof("ContractResult GasUsed:%+v", contractResult.GasUsed)
	loggerByChain.Infof("ContractResult Message:%+v", contractResult.Message)
	loggerByChain.Infof("ContractResult Result:%+X", contractResult.Result)
}

func TestInvokeC(t *testing.T) {
	test.ContractName = contractCName
	test.ByteCodeFile = contractCBody
	test.CertFilePath = certFilePath
	parameters := make(map[string][]byte)
	contractId, txContext, byteCode := test.InitContextTest(common.RuntimeType_EVM, t)

	runtimeInstance := &RuntimeInstance{
		ChainId:      chainId,
		Log:          logger.GetLogger(logger.MODULE_VM),
		TxSimContext: txContext,
	}

	loggerByChain := logger.GetLoggerByChain(logger.MODULE_VM, chainId)

	//调用合约
	abiJson, err := ioutil.ReadFile(contractCAbi)
	if err != nil {
		loggerByChain.Errorf("Read C ABI file failed, err:%v", err.Error())
	}

	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	if err != nil {
		loggerByChain.Errorf("constrcut ABI obj failed, err:%v", err.Error())
	}

	dataByte, err := myAbi.Pack("createDSalted", 5, "contract_D")
	if err != nil {
		loggerByChain.Errorf("create ABI data failed, err:%v", err.Error())
	}

	dataString := hex.EncodeToString(dataByte)
	method := dataString[0:8]
	//method2 := hex.EncodeToString(evmutils.Keccak256([]byte("createDSalted(uint256,string)")))[0:8]
	//loggerByChain.Infof("method:%v, method2:%v", method, method2)

	//method := "a339d707"
	//dataString := "a339d70700000000000000000000000000000000000000000000000000000000000000050000000000000000000000000000000000000000000000000000000000000040000000000000000000000000000000000000000000000000000000000000000a636f6e74726163745f4400000000000000000000000000000000000000000000"

	byteCode, _ = hex.DecodeString(string(byteCode))
	test.BaseParam(parameters)
	parameters[protocol.ContractCreatorPkParam] = contractId.Creator.MemberInfo
	parameters[protocol.ContractSenderPkParam] = txContext.GetSender().MemberInfo
	parameters[protocol.ContractEvmParamKey] = []byte(dataString)

	contractResult, _ := runtimeInstance.Invoke(contractId, method, byteCode, parameters, txContext, 0)
	loggerByChain.Infof("method store-- ContractResult Code:%+v", contractResult.Code)
	loggerByChain.Infof("method store-- ContractResult ContractEvent:%+v", contractResult.ContractEvent)
	loggerByChain.Infof("method store-- ContractResult GasUsed:%+v", contractResult.GasUsed)
	loggerByChain.Infof("method store-- ContractResult Message:%+v", contractResult.Message)
	loggerByChain.Infof("method store-- ContractResult Result:%+X", contractResult.Result)
}

//func TestConvertEvmContractName(t *testing.T) {
//	name := "0x7162629f540a9e19eCBeEa163eB8e48eC898Ad00"
//	addr, _ := contractNameToAddress(name)
//	t.Logf("evm addr:%s", addr.Text(16))
//	assert.Equal(t, strings.ToLower(name[2:]), addr.Text(16))
//}

func mockSender() *pbac.Member {
	file, err := ioutil.ReadFile(certFilePath)
	if err != nil {
		panic("file is nil" + err.Error())
	}

	return &pbac.Member{
		OrgId:      OrgId1,
		MemberType: pbac.MemberType_CERT,
		MemberInfo: file,
	}
}

func mockContract(name string, cert []byte) *common.Contract {
	addr, _ := utils.NameToAddrStr(name, configPb.AddrType_ETHEREUM, 2300)

	return &common.Contract{
		Name:        name,
		Version:     contractVersion,
		RuntimeType: common.RuntimeType_EVM,
		Status:      common.ContractStatus_NORMAL,
		Creator: &pbac.MemberFull{
			OrgId:      OrgId1,
			MemberType: pbac.MemberType_CERT,
			MemberInfo: cert,
		},
		Address: addr,
	}
}

func mockTx() *common.Transaction {
	return &common.Transaction{
		Payload: &common.Payload{
			ChainId:        chainId,
			TxType:         common.TxType_INVOKE_CONTRACT,
			TxId:           txId,
			Timestamp:      0,
			ExpirationTime: 0,
		},
		Result: nil,
	}
}

func mockParams(cert *bcx509.Certificate) map[string][]byte {
	parameters := make(map[string][]byte)

	parameters[protocol.ContractTxIdParam] = []byte(txId)
	parameters[protocol.ContractCreatorOrgIdParam] = []byte(OrgId1)
	parameters[protocol.ContractCreatorRoleParam] = []byte("admin")
	parameters[protocol.ContractCreatorPkParam] = []byte(hex.EncodeToString(cert.SubjectKeyId))
	parameters[protocol.ContractSenderOrgIdParam] = []byte(OrgId1)
	parameters[protocol.ContractSenderRoleParam] = []byte("user")
	parameters[protocol.ContractSenderPkParam] = []byte(hex.EncodeToString(cert.SubjectKeyId))
	parameters[protocol.ContractBlockHeightParam] = []byte("1")

	return parameters
}

//func TestInvoke(t *testing.T) {
//	sender := mockSender()
//	cert, _ := utils.ParseCert(sender.MemberInfo)
//	ctrl := gomock.NewController(t)
//
//	member := mock.NewMockMember(ctrl)
//	member.EXPECT().GetUid().Return(hex.EncodeToString(cert.SubjectKeyId)).AnyTimes()
//	member.EXPECT().GetPk().Return(cert.PublicKey).AnyTimes()
//	ac := mock.NewMockAccessControlProvider(ctrl)
//	ac.EXPECT().NewMember(gomock.Any()).Return(member, nil).AnyTimes()
//
//	ctx := mock.NewMockTxSimContext(ctrl)
//	ctx.EXPECT().GetAccessControl().Return(ac, nil).AnyTimes()
//
//	cfg := &configPb.ChainConfig{Vm: &configPb.Vm{AddrType: configPb.AddrType_ETHEREUM}}
//	bcs := mock.NewMockBlockchainStore(ctrl)
//	bcs.EXPECT().GetLastChainConfig().Return(cfg, nil).AnyTimes()
//	ctx.EXPECT().GetBlockchainStore().Return(bcs).AnyTimes()
//
//	ctx.EXPECT().GetBlockVersion().Return(uint32(2300)).AnyTimes()
//	ctx.EXPECT().GetBlockTimestamp().Return(time.Now().UnixNano()).AnyTimes()
//	ctx.EXPECT().GetBlockHeight().Return(uint64(1)).AnyTimes()
//	ctx.EXPECT().GetSender().Return(sender).AnyTimes()
//	ctx.EXPECT().GetCreator(storeName).Return(sender).AnyTimes()
//	ctx.EXPECT().GetTx().Return(mockTx()).AnyTimes()
//
//	loggerByChain := logger.GetLoggerByChain(logger.MODULE_VM, chainId)
//
//	//调用合约
//	abiJson, err := ioutil.ReadFile(storeAbiFile)
//	if err != nil {
//		loggerByChain.Errorf("Read C ABI file failed, err:%v", err.Error())
//	}
//
//	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
//	if err != nil {
//		loggerByChain.Errorf("constrcut ABI obj failed, err:%v", err.Error())
//	}
//
//	dataByte, err := myAbi.Pack("set", 5)
//	if err != nil {
//		loggerByChain.Errorf("create ABI data failed, err:%v", err.Error())
//	}
//
//	dataString := hex.EncodeToString(dataByte)
//	method := dataString[0:8]
//
//	contractId := mockContract(storeName, sender.MemberInfo)
//	bytes, _ := ioutil.ReadFile(storeBinFile)
//	byteCode, _ := hex.DecodeString(string(bytes))
//	parameters := mockParams(cert)
//	parameters[protocol.ContractEvmParamKey] = []byte(dataString)
//
//	runtimeInstance := &RuntimeInstance{
//		ChainId:      chainId,
//		Log:          logger.GetLogger(logger.MODULE_VM),
//		TxSimContext: ctx,
//	}
//
//	contractResult, _ := runtimeInstance.Invoke(contractId, method, byteCode, parameters, ctx, 0)
//	loggerByChain.Infof("method store-- ContractResult Code:%+v", contractResult.Code)
//	loggerByChain.Infof("method store-- ContractResult ContractEvent:%+v", contractResult.ContractEvent)
//	loggerByChain.Infof("method store-- ContractResult GasUsed:%+v", contractResult.GasUsed)
//	loggerByChain.Infof("method store-- ContractResult Message:%+v", contractResult.Message)
//	loggerByChain.Infof("method store-- ContractResult Result:%+X", contractResult.Result)
//}
